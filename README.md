# Summer 2020 Work Diary

This repository is contains the log of the open-source contributions under [shogun.ml](https://www.shogun-toolbox.org/). These contributions were part of my attempt at GSoC'20 for which I wasn't selected. Regardless, we decided that to pursue the proposed features.

## Problem Statement

The goal was to introduce ["Large scale Kernel Approximate"](https://github.com/shogun-toolbox/shogun/wiki/GSoC-2020-project-approx-kernels) algorithms to the library. Methods like Support Vector Machine, utilize the "Kernel Trick" to construct sophisticated decision surface using linear models by project datapoints to infinite dimensional space. The "trick" factor is that, the calculation is carried out implicitly without having to project the datapoints at all. For more information, have a look at this [article](https://medium.com/analytics-vidhya/a-layman-introduction-to-kernel-approximations-519ac9fe68d8) I've written published under Analytics Vidhya.

A major drawback of these methods is that it requires the calculation of the inverse of the Kernel Matrix which takes  $` \approx `$ $` O(N^3)`$ time. Since modern-day problems have very high number of datapoints, the storage and computation cost can be very expensive. To address this, newer generation algorithms use randomly sample this high dimensional space to approximate the Kernel Matrix.

My part of the work was to implement Random Fourier Features algorithm[2] along with some design refactoring and testing.


## Pull requests

* [Adding Random Fourier Gauss Preprocessor (RFGP)](https://github.com/shogun-toolbox/shogun/pull/5010).
* [Benchmarking RFGP](https://github.com/shogun-toolbox/shogun/pull/5012).
* [Multi-kernel framework for RFGP](https://github.com/shogun-toolbox/shogun/pull/5074).
* [Column iterator for Matrix class](https://github.com/shogun-toolbox/shogun/pull/4997).
* [Refactor Preprocessor module: Remove cleanup()](https://github.com/shogun-toolbox/shogun/pull/5091).
* [Refactor Preprocessor module: Add fit_impl()](https://github.com/shogun-toolbox/shogun/pull/5091).
* [Refactor Random Fourier Features Class](https://github.com/shogun-toolbox/shogun/pull/4955).
* [Cleanup Kernel constructors](https://github.com/shogun-toolbox/shogun/pull/5036)


## References
 
  Link for original problem statement: https://github.com/shogun-toolbox/shogun/wiki/GSoC-2020-project-approx-kernels.

  Link for original proposal: https://drive.google.com/file/d/1NkjUjndKN1wtUYZwr8-XC8UcyV9jKGnc/view?usp=sharing.
## Relevant papers

1. Alaoui, Ahmed El, and Michael W. Mahoney. "Fast randomized kernel methods with statistical guarantees." arXiv preprint arXiv:1411.0306 (2014).
2. Rahimi, Ali, and Benjamin Recht. "Random Features for Large-Scale Kernel Machines." NIPS. Vol. 3. No. 4. 2007.



